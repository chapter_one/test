package chptr.life;

// Это клетка. У нее всего два состояния. Тут все очевидно.
public enum Cell {
    DEAD,
    ALIVE
}
